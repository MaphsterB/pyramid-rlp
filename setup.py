#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import sys

from setuptools import find_packages, setup

##
# Basic information
##
NAME = "pyramid-rlp"
DESCRIPTION = "REST API launchpad for Python & Pyramid"
URL = "https://gitlab.com/MaphsterB/pyramid-rlp"
EMAIL = "docmaph@gmail.com"
AUTHOR = "MaphsterB"
REQUIRES_PYTHON = ">=3.4.0"
VERSION = None


##
# Information loaded from project files
##
here = os.path.abspath(os.path.dirname(__file__))

try:
    with open(os.path.join(here, "README.md"), encoding="utf-8") as f:
        long_description = "\n" + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION

about = {}
try:
    with open(os.path.join(here, NAME.replace("-", "_"), "__version__.py")) as f:
        exec(f.read(), about)
except FileNotFoundError:
    about["__version__"] = VERSION

comment = re.compile(r"^\s*#")
trailing_comment = re.compile(r"\s*#.*")
requirements = []
try:
    with open(os.path.join(here, "requirements.txt")) as f:
        requirements = [
            re.sub(trailing_comment, "", line.rstrip())
                for line in f.readlines()
                if not re.match(comment, line)
        ]
except FileNotFoundError:
    pass

extra_requirements = {}
try:
    with open(os.path.join(here, "requirements-optional.txt")) as f:
        extra_requirements["pandas support"] = [
            re.sub(trailing_comment, "", line.rstrip())
                for line in f.readlines()
                if not re.match(comment, line)
        ]
except FileNotFoundError:
    pass
try:
    with open(os.path.join(here, "requirements-dev.txt")) as f:
        extra_requirements["development"] = [
            re.sub(trailing_comment, "", line.rstrip())
                for line in f.readlines()
                if not re.match(comment, line)
        ]
except FileNotFoundError:
    pass


##
# The setup itself
##
setup(
    name=NAME,
    version=about["__version__"],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type="text/markdown",
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=find_packages(exclude=("tests",)),

    install_requires=requirements,
    extras_require=extra_requirements,
    include_package_data=True,
    license="MIT",
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 1 - Planning",
        "Environment :: Web Environment",
        "Framework :: Pyramid",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: Implementation :: CPython",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
        "Topic :: Utilities",
    ]
)
