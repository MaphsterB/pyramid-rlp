# pyramid-rlp

Thoughts on a(nother) REST API framework for Python, built atop Pyramid+SQLAlchemy.

Currently just an attempt; opinionated; pre-alpha status; etc. etc.

RLP = REST LaunchPad, BTW.


## Project Goals

* Support Python 3.4+
* Support Pyramid 1.7+
* Declarative (decorator-based) syntax
* Toolkit-like:
  * Pyramid cookiecutter for fast project bootstrapping
  * Extension tools to craft an API
  * Layered & customizable: devs can easily modify or extend the toolkit to suit their needs
* Support for SQLAlchemy as the model for the backing data store
  * Initially, not aiming for any dialects
  * First dialect-specific additions: Postgres, MySQL, sqlite, MSSQL
* Extension for numpy data types
* Extension to support pandas as the (or an additional) backing data store
* Serializers for a number of common Content-Types
* Handle CORS preflights (and requests) appropriately & transparently
* HATEOAS (full REST) with minimal extra effort
* Minimal External dependencies (so few, we will maintain a list here):
  * Pyramid
  * SQLAlchemy
  * json_tricks (for easier serialization)
  * optional: numpy, pandas
  * dev: cookiecutter, pylint, pytest, pytest-cov, pytest-mock


## Getting Started

```bash
# Clone the repo
git clone https://gitlab.com/MaphsterB/pyramid-rlp.git

# Create a virtual environment
# With virtualenv...
pip install virtualenv
virtualenv venv && source venv/bin/activate
# Or with conda...
conda create -n myenv python=3.6

# Install requirements
pip install requirements.txt
pip install requirements-dev.txt

# Install pyramid-rlp in dev mode
pip install -e .

# Optional: Install git hooks
./hooks/install_hooks
# Or on Windows...
./hooks/install_hooks.cmd
```

### Notes

* We use [pylint](https://www.pylint.org/) to lint the project; right now, the `.pylintrc`
  file still hasn't been "calmed down" to the point where it is not super-strict (the default),
  since not much code has been written yet.

* We use [pytest](https://docs.pytest.org/en/latest/index.html) for testing. The goal is to
  maintain a high level (> 90%) of coverage. Tests are located in `pyramid_rlp/tests`. The
  `.coveragerc` file is set up to provide missing line reports by default.

* The `test` stage of CI jobs is set up to lint & test each push.

* The `pre-commit` hook is set up to lint the project & test with coverage by default.


## License

This project is licensed under the [MIT License](https://choosealicense.com/licenses/mit/).
