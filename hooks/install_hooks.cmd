@echo off

set hooks=applypatch-msg^
 commit-msg^
 post-update^
 pre-applypatch^
 pre-commit^
 pre-push^
 pre-rebase^
 prepare-commit-msg^
 update

for %%h in (%hooks%) do (
    if exist hooks\%%h (
        rem Windows symlinks don't work w/ git >_<
        rem mklink .git\hooks\%%h hooks\%%h
        copy /y hooks\%%h .git\hooks\%%h
    )
)
