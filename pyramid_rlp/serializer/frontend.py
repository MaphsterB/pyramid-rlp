"""
SQLAlchemy model serializer frontend: Take a model object and
generate a dict containing only collections & Python primitive
types. Do so recursively, and configurably (e.g. terminate
after N levels).

Such a structure is easily translated into other serialization
formats, e.g. JSON, pickle.
"""
# pylint:disable=protected-access
# Yes, we will be digging around inside SQLAlchemy here.

from collections import Iterable, Mapping
import datetime as dt
import enum

import json_tricks as jt


# Config variables
# Globals, for now.
DATE_FORMAT = "%Y-%m-%d" # Also available: "dict" to jsonify all fields.
TIME_FORMAT = "%H:%M:%S.%f" # Ditto above.
DATETIME_FORMAT = " ".join((DATE_FORMAT, TIME_FORMAT)) # Ditto above.
ENUM_FORMAT = "value" # alternative: 'name' ; what to use for enum types: name or value.


class ModelPlaceholder:
    """
    Representation of a model that will not be serialized, because it was
    already noted by the cycle-breaking code. Allows usage of things like
    YAML anchors and is a clearer indication of what's going on than
    generating None (or not generating a field at all) while serializing.
    """
    # pylint:disable=too-few-public-methods
    def __init__(self, mod_obj):
        self.reference = mod_obj

    def __eq__(self, other):
        """
        For testability: two placeholders referring to the same model
        are the same.
        """
        return id(self.reference) == id(other.reference)

    def __repr__(self):
        return "<ModelPlaceholder for {}>".format(repr(self.reference))


def _dt_transform(fmt):
    """
    Create a transformation function for types from the datetime module.
    The transform returns a string representaton of the object, unless
    fmt is "dict," in which case it will be serialized as a dict via
    json_tricks.dumps().
    """
    def transform(val):
        fmt_val = globals()[fmt]
        return (val.strftime(fmt_val)
                if fmt_val.lower() != "dict"
                else jt.dumps(val))
    return transform


_SCALAR_TRANSFORMS = {
    bytes: lambda x: "0x" + x.hex(),
    dt.date: _dt_transform("DATE_FORMAT"),
    dt.time: _dt_transform("TIME_FORMAT"),
    dt.datetime: _dt_transform("DATETIME_FORMAT"),
}


# TODO public functions to register/unregister transforms


def serialize_model_object(mod_obj, columns=None, relationships=None, attributes=None):
    """
    Main serialization function. Given a SQLAlchemy model object,
    provides a data structure consisting of pickle-able Python types
    (dicts, lists, etc.), assuming it contains no unsupported data types
    (custom objects for which no scalar transform has been registered, etc.).

    This intermediate format may easily be serialized into many different
    formats, with the primary target being JSON.
    """
    return dict(_serialize_model_object(mod_obj, set(), columns, relationships, attributes))


def _serialize_model_object(mod_obj, rels_seen, columns=None, relationships=None, attributes=None):
    """
    Internal, recursive version of the serialization function.
    """
    mod_cols = _get_model_columns(mod_obj, columns)
    for (col, out_name) in mod_cols.items():
        yield (out_name, _serialize_attribute(getattr(mod_obj, col.name)))
    mod_rels = _get_model_relationships(mod_obj, relationships)
    for (rel, out_name) in mod_rels.items():
        yield from _serialize_relationship(mod_obj, rel, out_name, rels_seen)
    mod_attrs = _get_model_attributes(mod_obj, attributes)
    for (attr, out_name) in mod_attrs.items():
        attr_val = getattr(mod_obj, attr)
        if hasattr(attr_val, "__call__"):
            attr_val = attr_val()
        yield (out_name, _serialize_attribute(attr_val))


def _get_model_columns(model, columns=None):
    """
    Determine which columns of a model class will be serialized, and
    their output names. Return a dict of `{Column: output_name}`
    given the model and optional user specifications for column selection.

    columns can be:
        mapping(dict-like): Manually specify columns and mapping {input_name: output_name}
        iterable(list-like): Only the specified columns will be mapped directly
        None: All columns will be mapped directly
    """
    if columns is None:
        columns = getattr(model, "__serialize_columns__", None)
    if isinstance(columns, Mapping):
        return {
            model.__table__.columns[k]: v
            for (k, v) in columns.items()
        }
    if isinstance(columns, Iterable):
        return {
            model.__table__.columns[c]: c
            for c in columns
        }
    return {c: c.name for c in model.__table__.columns}


def _get_model_relationships(model, relationships=None):
    """
    Determine which relationships of a model class will be serialized, and
    their output names. Return a dict of `{RelationshipProperty: output_name}`
    given the model and optional user specifications for relationship selection.

    relationships can be:
        mapping(dict-like): Manually specify relationships and mapping
        iterable(list-like): Only the specified relationships will be mapped directly
        None: Only eager-loaded relationships will be mapped directly
    """
    if relationships is None:
        relationships = getattr(model, "__serialize_relationships__", None)
    if isinstance(relationships, Mapping):
        return {
            model.__mapper__.relationships[k]: v
            for (k, v) in relationships.items()
        }
    if isinstance(relationships, Iterable):
        return {
            model.__mapper__.relationships[r]: r
            for r in relationships
        }
    return {
        r: r.key for r in model.__mapper__.relationships
        if r.lazy in (False, "immediate", "joined", "subquery", "selectin")
    }


def _get_model_attributes(model, attributes=None):
    """
    Determine which other attributes of a model class will be serialized. This
    is entirely manually specified by users; by default no attributes are
    included. Methods & properties can be specified; methods will be called
    to return a value; properties will get got. Return a dict of
    `{attribute_name: output_name}`. Will automatically exclude any specified
    attribute that is already a column or relationship.

    attributes can be:
        mapping(dict-like): Manually specify attributes and mapping
        iterable(list-like): Only the specified attributes will be mapped directly
        None: No attributes will be serialized
    """
    if attributes is None:
        attributes = getattr(model, "__serialize_attributes__", None)
    if isinstance(attributes, Mapping):
        colnames = {c.name for c in model.__table__.columns}
        relnames = {r.key for r in model.__mapper__.relationships}
        return {
            k: v for (k, v) in attributes.items()
            if getattr(model, k) is not None
            and k not in colnames
            and k not in relnames
        }
    if isinstance(attributes, Iterable):
        colnames = {c.name for c in model.__table__.columns}
        relnames = {r.key for r in model.__mapper__.relationships}
        return {
            a: a for a in attributes
            if getattr(model, a) is not None
            and a not in colnames
            and a not in relnames
        }
    return {}


def _serialize_attribute(obj):
    """
    General serialization of attributes. May recurse back to _serialize_model_object()
    if the attribute value is determined to be a model object.
    Recursive for collections: dicts stay dicts, sets become frozensets;
    all other non-string iterables become tuples.
    """
    if hasattr(obj, "__table__") or hasattr(obj, "__tablename__"):
        return dict(_serialize_model_object(obj, set()))
    if isinstance(obj, Mapping):
        return {
            _serialize_attribute(k): _serialize_attribute(v)
            for (k, v) in obj.items()
        }
    if isinstance(obj, Iterable) and not isinstance(obj, (str, bytes)):
        if hasattr(obj, "__and__"):
            return frozenset(_serialize_attribute(x) for x in obj)
        return tuple(_serialize_attribute(x) for x in obj)
    return _serialize_scalar(obj)


def _serialize_scalar(obj):
    """
    Serialize non-model, non-collection. If we have a transform
    callable registered for its type, transform it. Treat Enums
    specially (unless registered), since we can't transform by
    superclass lookup.
    """
    # pylint:disable=unidiomatic-typecheck
    # pylint is incorrect; we can't use isinstance() in place of this type() call.
    if type(obj) in _SCALAR_TRANSFORMS:
        return _SCALAR_TRANSFORMS[type(obj)](obj)
    if isinstance(obj, enum.Enum):
        return obj.name if ENUM_FORMAT == "name" else obj.value
    return obj


def _serialize_relationship(mod_obj, rel, out_name, rels_seen):
    """
    Serialize the model object(s) on the other end of a relationship.
    rels_seen serves as a cycle-breaking set tracking which relationships
    we have already processed. It also prevents both directions of a
    bidirectional relationship from being processed.
    """
    rel_val = getattr(mod_obj, rel.key)
    if rel_val is None:
        yield (out_name, None)
    elif rel.uselist:
        yield (out_name, [
            dict(_serialize_model_object(other_obj, rels_seen))
            if _check_rels_seen(rel, mod_obj, other_obj, rels_seen)
            else ModelPlaceholder(other_obj)
            for other_obj in rel_val
        ])
    elif _check_rels_seen(rel, mod_obj, rel_val, rels_seen):
        yield (out_name, dict(_serialize_model_object(rel_val, rels_seen)))
    else:
        yield (out_name, ModelPlaceholder(rel_val))


def _check_rels_seen(rel, mod_obj, other_obj, rels_seen):
    """
    The criteria for whether we render a relationship is:
        * We haven't seen it before, AND...
        * We haven't seen its reverse either.
    We identify a relationship between 2 model objects as the
    3-tuple: (RelationshipProperty, Model1, Model2) and
    use the private _reverse_property property of the
    RelationshipProperty (er... yep) to get the reverse relationship.
    """
    rev = list(rel._reverse_property)[0] if rel._reverse_property else None
    fwd_tuple = (id(rel), id(mod_obj), id(other_obj))
    rev_tuple = (id(rev), id(other_obj), id(mod_obj))
    if fwd_tuple in rels_seen or rev_tuple in rels_seen:
        return False
    rels_seen |= {fwd_tuple, rev_tuple}
    return True
