"""
Unit & integration tests for the serializer frontend.

We don't mock out any SQLAlchemy functionality for these
tests; that is intentional & due to the fact that the serializer's
functionality kind of digs into SQLAlchemy's guts.

Because the serializer has complex recursion, we take it apart
and test its private functions individually where possible.

Tests go "bottom-up," starting with basic tests of finding
columns, relationships, and attributes. Then it tests serializing
scalars, then collections. Then it tests basic relationship patterns,
and finally it tests the top-level recursive serialize_model function.
"""
# pylint:disable=too-many-public-methods
# pylint:disable=too-few-public-methods
# pylint:disable=protected-access
# pylint:disable=invalid-name
# pylint:disable=redefined-outer-name

import datetime as dt
import enum

import pytest

from sqlalchemy import (
    Table,
    Column,
    ForeignKey,
    Boolean,
    Date,
    DateTime,
    Enum,
    Float,
    Integer,
    Interval,
    LargeBinary,
    Numeric,
    String,
    Time
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

import pyramid_rlp.serializer.frontend as fe


class MyEnum(enum.Enum):
    """Enum for testing sa.Enum column type"""
    one = 1
    two = 2
    three = 3


@pytest.fixture
def Base():
    """Fixture: SQLAlchemy ORM declarative base"""
    return declarative_base()


@pytest.fixture
def Model1(Base):
    """
    Fixture: Model class with all of the different
    column types we care about serializing.
    """
    class _Model1(Base):
        __tablename__ = "table1"
        id = Column(Integer, primary_key=True)
        col1 = Column(Boolean)
        col2 = Column(Date)
        col3 = Column(DateTime(timezone=False))
        col4 = Column(DateTime(timezone=True))
        col5 = Column(Enum(MyEnum))
        col6 = Column(Float)
        col7 = Column(Integer)
        col8 = Column(Interval)
        col9 = Column(LargeBinary)
        col10 = Column(Numeric)
        col11 = Column(String)
        col12 = Column(Time(timezone=False))
        col13 = Column(Time(timezone=True))
    return _Model1


@pytest.fixture
def Model2(Base, Model1):
    """
    Fixture: Model class with all the different types of
    relationship loading (but all of them are
    one-to-one)
    """
    class _Model2(Base):
        __tablename__ = "table2"
        id = Column(Integer, primary_key=True)
        fk1 = Column(Integer, ForeignKey(Model1.id))
        fk2 = Column(Integer, ForeignKey(Model1.id))
        fk3 = Column(Integer, ForeignKey(Model1.id))
        fk4 = Column(Integer, ForeignKey(Model1.id))
        fk5 = Column(Integer, ForeignKey(Model1.id))
        fk6 = Column(Integer, ForeignKey(Model1.id))
        fk7 = Column(Integer, ForeignKey(Model1.id))
        fk8 = Column(Integer, ForeignKey(Model1.id))
        fk9 = Column(Integer, ForeignKey(Model1.id))
        fk10 = Column(Integer, ForeignKey(Model1.id))
        fk11 = Column(Integer, ForeignKey(Model1.id))
        fk12 = Column(Integer, ForeignKey(Model1.id))
        # These should be picked up as eager-loaded
        rel1 = relationship(Model1, uselist=False, foreign_keys=[fk1], lazy=False)
        rel2 = relationship(Model1, uselist=False, foreign_keys=[fk2], lazy="immediate")
        rel3 = relationship(Model1, uselist=False, foreign_keys=[fk3], lazy="joined")
        rel4 = relationship(Model1, uselist=False, foreign_keys=[fk4], lazy="subquery")
        rel5 = relationship(Model1, uselist=False, foreign_keys=[fk5], lazy="selectin")
        # These should not be picked up as eager-loaded
        rel6 = relationship(Model1, uselist=False, foreign_keys=[fk6], lazy=True)
        rel7 = relationship(Model1, uselist=False, foreign_keys=[fk7], lazy=None)
        rel8 = relationship(Model1, uselist=False, foreign_keys=[fk8], lazy="select")
        rel9 = relationship(Model1, uselist=False, foreign_keys=[fk9], lazy="noload")
        rel10 = relationship(Model1, uselist=False, foreign_keys=[fk10], lazy="raise")
        rel11 = relationship(Model1, uselist=False, foreign_keys=[fk11], lazy="raise_on_sql")
        rel12 = relationship(Model1, uselist=True, foreign_keys=[fk12], lazy="dynamic")
    return _Model2


@pytest.fixture
def Model3(Base, Model1):
    """
    Fixture: Model class with various properties & attributes.
    """
    # pylint:disable=missing-docstring
    # pylint:disable=no-self-use
    class _Model3(Base):
        __tablename__ = "table3"
        id = Column(Integer, primary_key=True)
        fk = Column(Integer, ForeignKey(Model1.id))
        rel = relationship(Model1, uselist=False)

        attr1 = 42

        def attr2(self):
            return 42

        @property
        def attr3(self):
            return 42

        @attr3.setter
        def attr3(self, value):
            pass

        def __attr4(self):
            pass

        def __str__(self):
            return "Model3"
    return _Model3


def test_model_placeholder_repr(Model1):
    """
    Misc. test case for coverage.
    Call ModelPlaceholder.__repr__().
    """
    x = Model1(id=1)
    mp = fe.ModelPlaceholder(x)
    assert repr(mp) == "<ModelPlaceholder for {}>".format(repr(x))



class TestFindColumns:
    """
    Test grouping for _get_model_columns().
    """

    @staticmethod
    def test_find_columns_with_defaults(Model1):
        """
        Make sure _get_model_columns() finds all columns by default.
        Note: We're testing all column types here, too. Type shouldn't
        affect our ability to find columns, but it doesn't hurt to
        check while we test defaults, too.
        """
        result = fe._get_model_columns(Model1)
        names = {c.name for c in result.keys()}
        values = set(result.values())
        correct = {"id"} | {f"col{n}" for n in range(1, 14)}
        assert names == correct
        assert names == values

    @staticmethod
    def test_find_columns_with_list(Model1):
        """
        Make sure _get_model_columns() works when passed a list
        of column names.
        """
        correct = {"col1", "col3", "col5"}
        result = fe._get_model_columns(Model1, correct)
        names = {c.name for c in result.keys()}
        values = set(result.values())
        assert names == correct
        assert names == values

    @staticmethod
    def test_find_columns_with_map(Model1):
        """
        Make sure _get_model_columns() works when passed a
        map of input column names to output names.
        """
        mapping = {"col1": "out1", "col2": "out2", "col3": "out3"}
        result = fe._get_model_columns(Model1, mapping)
        names = {c.name for c in result.keys()}
        values = set(result.values())
        assert names == set(mapping.keys())
        assert values == set(mapping.values())

    @staticmethod
    def test_find_columns_with_bad_name_in_list(Model1):
        """
        Make sure a KeyError is raised when trying to specify a
        column name that doesn't exist.
        """
        with pytest.raises(KeyError):
            _ = fe._get_model_columns(Model1, ["bogus"])

    @staticmethod
    def test_find_columns_with_bad_name_in_map(Model1):
        """Ditto previous test, with a map instead of a list."""
        with pytest.raises(KeyError):
            _ = fe._get_model_columns(Model1, {"bogus": "output"})

    @staticmethod
    def test_find_columns_with_name_mangled_model(Base):
        """
        Test how name mangling affects our ability to locate columns.
        Currently, columns are mangled like all object attributes.
        This test will fail if that ever changes.
        """
        class Mangled(Base):
            """Inner test class"""
            __tablename__ = "mangled"
            __id = Column(Integer, primary_key=True)
        result = fe._get_model_columns(Mangled, ["_Mangled__id"])
        assert list(result.keys())[0].name == "_Mangled__id"
        assert list(result.values())[0] == "_Mangled__id"

    @staticmethod
    def test_find_columns_from_class_attribute(Model1):
        """
        _get_model_columns() also allows the columns to be specified
        from the __serialize_columns__ class attribute of a model
        class.
        """
        correct = {"col1", "col3", "col5"}
        Model1.__serialize_columns__ = correct
        result = fe._get_model_columns(Model1)
        names = {c.name for c in result.keys()}
        values = set(result.values())
        assert names == correct
        assert names == values

    @staticmethod
    def test_columns_arg_overrides_class_attribute(Model1):
        """
        An argument manually passed into _get_model_columns()
        should take precedence over the __serialize_columns__
        class attribute.
        """
        correct = {"col1", "col3", "col5"}
        incorrect = {"col2", "col4", "col6"}
        Model1.__serialize_columns__ = incorrect
        result = fe._get_model_columns(Model1, correct)
        names = {c.name for c in result.keys()}
        values = set(result.values())
        assert names == correct
        assert names == values


class TestFindRelationships:
    """
    Test grouping for _get_model_relationships().
    """

    @staticmethod
    def test_find_relationships_with_defaults(Model2):
        """
        Make sure _get_model_relationships() locates all relationships
        configured for eager-loading by default.
        """
        result = fe._get_model_relationships(Model2)
        names = {r.key for r in result.keys()}
        values = set(result.values())
        correct = {f"rel{n}" for n in range(1, 6)}
        assert names == correct
        assert names == values

    @staticmethod
    def test_find_relationships_with_list(Model2):
        """
        Make sure _get_model_relationships() works when passed a list
        of relationship names.
        """
        correct = {f"rel{n}" for n in range(1, 12)}
        result = fe._get_model_relationships(Model2, correct)
        names = {r.key for r in result.keys()}
        values = set(result.values())
        assert names == correct
        assert names == values

    @staticmethod
    def test_find_relationships_with_mapping(Model2):
        """
        Make sure _get_model_relationships() works when passed a
        map of input relationship names to output names.
        """
        mapping = {f"rel{n}": f"out{n}" for n in range(1, 12)}
        result = fe._get_model_relationships(Model2, mapping)
        names = {r.key for r in result.keys()}
        values = set(result.values())
        assert names == set(mapping.keys())
        assert values == set(mapping.values())

    @staticmethod
    def test_find_relationships_with_bad_name_in_list(Model2):
        """
        Make sure a KeyError is raised when trying to specify a
        relationship name that doesn't exist.
        """
        with pytest.raises(KeyError):
            _ = fe._get_model_relationships(Model2, ["bogus"])

    @staticmethod
    def test_find_relationships_with_bad_name_in_map(Model2):
        """Ditto previous test, with a map instead of a list."""
        with pytest.raises(KeyError):
            _ = fe._get_model_relationships(Model2, {"bogus": "output"})

    @staticmethod
    def test_find_relationships_with_name_mangled_model(Base, Model1):
        """
        Test how name mangling affects our ability to locate relationships.
        Currently, relationships are mangled like all object attributes.
        This test will fail if that ever changes.
        """
        class Mangled(Base):
            """Inner test class"""
            __tablename__ = "mangled"
            id = Column(Integer, primary_key=True)
            fk = Column(Integer, ForeignKey(Model1.id))
            __rel = relationship(Model1, uselist=False, lazy=False)
        result = fe._get_model_relationships(Mangled, ["_Mangled__rel"])
        assert list(result.keys())[0].key == "_Mangled__rel"
        assert list(result.values())[0] == "_Mangled__rel"

    @staticmethod
    def test_find_relationships_from_class_attribute(Model2):
        """
        _get_model_relationships() also allows the rels to be specified
        from the __serialize_relationships__ class attribute of a model
        class.
        """
        correct = {"rel1", "rel3", "rel5"}
        Model2.__serialize_relationships__ = correct
        result = fe._get_model_relationships(Model2)
        names = {r.key for r in result.keys()}
        values = set(result.values())
        assert names == correct
        assert names == values

    @staticmethod
    def test_relationships_arg_overrides_class_attribute(Model2):
        """
        An argument manually passed into _get_model_relationships()
        should take precedence over the __serialize_relationships__
        class attribute.
        """
        correct = {"rel1", "rel3", "rel5"}
        incorrect = {"rel2", "rel4", "rel6"}
        Model2.__serialize_relationships__ = incorrect
        result = fe._get_model_relationships(Model2, correct)
        names = {r.key for r in result.keys()}
        values = set(result.values())
        assert names == correct
        assert names == values


class TestFindAttributes:
    """
    Test grouping for _get_model_attributes().
    """

    @staticmethod
    def test_find_attributes_with_defaults(Model3):
        """
        Confirm that _get_model_attributes() with no args returns
        an empty collection by default.
        """
        # pylint: disable=len-as-condition
        # We want to explicitly check length in this case.
        result = fe._get_model_attributes(Model3)
        assert len(result) == 0

    @staticmethod
    def test_find_attributes_with_list(Model3):
        """
        Make sure _get_model_attributes() works when passed a list.
        Test several types of attributes (class, method, property, magic)
        and also verify that if we specify a column or relationship, it's
        excluded.
        """
        attrs = [
            "__tablename__", "id", "fk", "rel",
            "attr1", "attr2", "attr3", "_Model3__attr4",
            "__str__"
        ]
        correct = {
            "__tablename__", "attr1", "attr2", "attr3",
            "_Model3__attr4", "__str__"
        }
        result = fe._get_model_attributes(Model3, attrs)
        assert set(result.keys()) == correct
        assert set(result.values()) == correct

    @staticmethod
    def test_find_attributes_with_mapping(Model3):
        """Ditto above, with custom mapping instead of list."""
        attrs = {
            "__tablename__": "table",
            "id": "id",
            "fk": "fk",
            "rel": "rel",
            "attr1": "out1",
            "attr2": "out2",
            "attr3": "out3",
            "_Model3__attr4": "out4",
            "__str__": "str",
        }
        correct_keys = {
            "__tablename__", "attr1", "attr2", "attr3",
            "_Model3__attr4", "__str__"
        }
        correct_values = {"table", "out1", "out2", "out3", "out4", "str"}
        result = fe._get_model_attributes(Model3, attrs)
        assert set(result.keys()) == correct_keys
        assert set(result.values()) == correct_values

    @staticmethod
    def test_find_attributes_with_bad_name_in_list(Model3):
        """
        Make sure an AttributeError is raised when trying to specify an
        attribute that doesn't exist.
        """
        with pytest.raises(AttributeError):
            _ = fe._get_model_attributes(Model3, ["bogus"])

    @staticmethod
    def test_find_attributes_with_bad_name_in_map(Model3):
        """
        Make sure an AttributeError is raised when trying to specify an
        attribute that doesn't exist.
        """
        with pytest.raises(AttributeError):
            _ = fe._get_model_attributes(Model3, {"bogus": "output"})

    @staticmethod
    def test_find_attributes_from_class_attribute(Model3):
        """
        _get_model_attributes() also allows the attrs to be specified
        from the __serialize_attributes__ class attribute of a model
        class.
        """
        correct = {"attr1", "attr3", "__str__"}
        Model3.__serialize_attributes__ = correct
        result = fe._get_model_attributes(Model3)
        names = set(result.keys())
        values = set(result.values())
        assert names == correct
        assert names == values

    @staticmethod
    def test_attributes_arg_overrides_class_attribute(Model3):
        """
        An argument manually passed into _get_model_attributes()
        should take precedence over the __serialize_attributes__
        class attribute.
        """
        correct = {"attr1", "attr3", "__str__"}
        incorrect = {"rel2", "rel4"}
        Model3.__serialize_attributes__ = incorrect
        result = fe._get_model_attributes(Model3, correct)
        names = set(result.keys())
        values = set(result.values())
        assert names == correct
        assert names == values


class TestSerializeScalars:
    """
    Basic serialization tests of scalar values.
    """

    @staticmethod
    def test_serialize_int():
        """Integers should pass straight through."""
        assert fe._serialize_attribute(42) == 42

    @staticmethod
    def test_serialize_str():
        """Strings should pass straight through."""
        assert fe._serialize_attribute("python") == "python"

    @staticmethod
    def test_serialize_float():
        """Floats should pass straight through."""
        assert fe._serialize_attribute(3.1415) == 3.1415

    @staticmethod
    def test_serialize_bool():
        """Booleans should pass straight through."""
        assert fe._serialize_attribute(True)
        assert not fe._serialize_attribute(False)

    @staticmethod
    def test_serialize_none():
        """None should pass straight through."""
        assert fe._serialize_attribute(None) is None

    @staticmethod
    def test_serialize_bytes():
        """Bytestrings should be hex'd, by default."""
        assert fe._serialize_attribute(bytes.fromhex("deadbeef")) == "0xdeadbeef"

    @staticmethod
    def test_serialize_date():
        """Dates should be stringified as Y-m-d, by default."""
        assert fe._serialize_attribute(dt.date(year=2018, month=1, day=2)) == "2018-01-02"

    @staticmethod
    def test_serialize_date_with_custom_format(mocker):
        """Dates should allow customized formatting."""
        mocker.patch.object(fe, "DATE_FORMAT", "%m/%d/%Y")
        d = dt.date(year=2018, month=1, day=2)
        assert fe._serialize_attribute(d) == "01/02/2018"

    @staticmethod
    def test_serialize_date_with_dict_format_mocked(mocker):
        """Dates should allow json_tricks dict formatting."""
        mocker.patch.object(fe, "DATE_FORMAT", "dict")
        with mocker.mock_module.patch.object(fe.jt, "dumps") as mock_dumps:
            d = dt.date.today()
            _ = fe._serialize_attribute(d)
            mock_dumps.assert_called_once_with(d)

    @staticmethod
    def test_serialize_date_with_dict_format(mocker):
        """As above, but don't mock json_tricks.dumps()."""
        mocker.patch.object(fe, "DATE_FORMAT", "dict")
        d = dt.date(year=2018, month=1, day=2)
        correct = '{"__date__": null, "year": 2018, "month": 1, "day": 2}'
        assert fe._serialize_attribute(d) == correct

    @staticmethod
    def test_serialize_time():
        """Times should be stringified as H:M:S.f, by default."""
        t = dt.time(hour=13, minute=1, second=2, microsecond=123456)
        assert fe._serialize_attribute(t) == "13:01:02.123456"

    @staticmethod
    def test_serialize_time_with_custom_format(mocker):
        """Dates should allow customized formatting."""
        mocker.patch.object(fe, "TIME_FORMAT", "%S:%M:%H")
        t = dt.time(hour=13, minute=1, second=2)
        assert fe._serialize_attribute(t) == "02:01:13"

    @staticmethod
    def test_serialize_time_with_dict_format_mocked(mocker):
        """Times should allow json_tricks dict formatting."""
        mocker.patch.object(fe, "TIME_FORMAT", "dict")
        with mocker.mock_module.patch.object(fe.jt, "dumps") as mock_dumps:
            t = dt.datetime.now().time()
            _ = fe._serialize_attribute(t)
            mock_dumps.assert_called_once_with(t)

    @staticmethod
    def test_serialize_time_with_dict_format(mocker):
        """As above, but don't mock json_tricks.dumps()."""
        mocker.patch.object(fe, "TIME_FORMAT", "dict")
        t = dt.time(hour=13, minute=1, second=2, microsecond=123456)
        correct = '{"__time__": null, "hour": 13, "minute": 1, "second": 2, "microsecond": 123456}'
        assert fe._serialize_attribute(t) == correct

    @staticmethod
    def test_serialize_datetime():
        """Datetimes should serialize as Y-m-d H:M:S.f by default."""
        d = dt.datetime(year=2018, month=1, day=2, hour=13, minute=42, second=0, microsecond=123456)
        assert fe._serialize_attribute(d) == "2018-01-02 13:42:00.123456"

    @staticmethod
    def test_serialize_datetime_with_custom_format(mocker):
        """Datetimes should allow customized formatting."""
        mocker.patch.object(fe, "DATETIME_FORMAT", "%b %d, %Y %I:%M:%S%p")
        d = dt.datetime(year=2018, month=1, day=2, hour=13, minute=42, second=0, microsecond=123456)
        assert fe._serialize_attribute(d).lower() == "jan 02, 2018 01:42:00pm"

    @staticmethod
    def test_serialize_datetime_with_dict_format_mocked(mocker):
        """Datetimes should allow json_tricks dict formatting."""
        mocker.patch.object(fe, "DATETIME_FORMAT", "dict")
        with mocker.mock_module.patch.object(fe.jt, "dumps") as mock_dumps:
            d = dt.datetime.now()
            _ = fe._serialize_attribute(d)
            mock_dumps.assert_called_once_with(d)

    @staticmethod
    def test_serialize_datetime_with_dict_format(mocker):
        """As above, but don't mock json_tricks.dumps()."""
        # pylint:disable=bad-continuation
        mocker.patch.object(fe, "DATETIME_FORMAT", "dict")
        d = dt.datetime(year=2018, month=1, day=2, hour=13, minute=42, second=1, microsecond=123456)
        correct = ''.join(('{',
            '"__datetime__": null, "year": 2018, "month": 1, "day": 2, '
            '"hour": 13, "minute": 42, "second": 1, "microsecond": 123456'
            '}'))
        assert fe._serialize_attribute(d) == correct

    @staticmethod
    def test_serialize_enum_in_name_mode(mocker):
        """Enums can serialize to the value's name, or value. Test name."""
        mocker.patch.object(fe, "ENUM_FORMAT", "name")
        a = MyEnum.one
        b = MyEnum.two
        c = MyEnum.three
        assert fe._serialize_attribute(a) == "one"
        assert fe._serialize_attribute(b) == "two"
        assert fe._serialize_attribute(c) == "three"

    @staticmethod
    def test_serialize_enum_in_value_mode(mocker):
        """Enums can serialize to the value's name, or value. Test value."""
        mocker.patch.object(fe, "ENUM_FORMAT", "value")
        a = MyEnum.one
        b = MyEnum.two
        c = MyEnum.three
        assert fe._serialize_attribute(a) == 1
        assert fe._serialize_attribute(b) == 2
        assert fe._serialize_attribute(c) == 3


class TestSerializeCollections:
    """
    Serialization test for _serialize_attribute() with collection values.
    """

    @staticmethod
    def test_serialize_list():
        """Test serializing a basic list, without recursion."""
        assert fe._serialize_attribute([1, 2, 3]) == (1, 2, 3)

    @staticmethod
    def test_serialize_list_recursion():
        """Test serializing a list of lists."""
        assert fe._serialize_attribute([[1, 2], [3, 4]]) == ((1, 2), (3, 4))

    @staticmethod
    def test_serialize_set():
        """Test set serialization."""
        assert fe._serialize_attribute({1, 2, 3}) == frozenset((1, 2, 3))

    @staticmethod
    def test_serialize_set_recursion():
        """Test serializing a set of tuples."""
        assert fe._serialize_attribute({(1, 2), (3, 4)}) == frozenset(((1, 2), (3, 4)))

    @staticmethod
    def test_serialize_dict():
        """Test dict serialization."""
        assert fe._serialize_attribute({"a": 1, "b": 2}) == {"a": 1, "b": 2}

    @staticmethod
    def test_serialize_dict_recursion():
        """Ensure we recurse into dict keys and values."""
        test = {
            dt.date(year=2018, month=1, day=2): [MyEnum.one, MyEnum.two, MyEnum.three],
            ((1, 2), (3, 4)): {1, 2, 3}
        }
        correct = {
            "2018-01-02": (1, 2, 3),
            ((1, 2), (3, 4)): frozenset((1, 2, 3))
        }
        assert fe._serialize_attribute(test) == correct

    @staticmethod
    def test_serialize_attr_recurses_to_serialize_model(mocker, Model1):
        """
        If an attribute has a model object as its value, _serialize_attribute()
        must detect that and call _serialize_model_object() on it.
        """
        with mocker.mock_module.patch.object(fe, "_serialize_model_object") as mock:
            mod_obj = Model1(id=1)
            _ = fe._serialize_attribute(mod_obj)
            mock.assert_called_once_with(mod_obj, set())


class TestSerializeRelationships:
    """
    Tests of _serialize_relationship().
    """

    @staticmethod
    def test_serialize_null_relationship(Model1, Model2):
        """Relationship values can be None. Confirm _serialize_relationship() doesn't choke."""
        m1 = Model1(id=1) # pylint:disable=unused-variable
        m2 = Model2(id=2)
        rel = Model2.__mapper__.relationships.rel1
        val = list(fe._serialize_relationship(m2, rel, "rel", set()))
        assert val == [("rel", None)]

    @staticmethod
    def test_serialize_relationship(Model1, Model2):
        """Test basic serialization of one-to-one relationship."""
        m1 = Model1(id=1)
        m2 = Model2(id=2, rel1=m1)
        rel = Model2.__mapper__.relationships.rel1
        val = list(fe._serialize_relationship(m2, rel, "rel", set()))
        correct = [("rel", {
            **{"id": 1}, **{f"col{n}": None for n in range(1, 14)}
        })]
        assert val == correct

    @staticmethod
    def test_serialize_two_way_relationship(Base):
        """
        Ensure we don't infinitely recurse when serializing a
        relationship that has back_populates specified.
        """
        # pylint:disable=missing-docstring
        class TestModel1(Base):
            __tablename__ = "test1"
            id = Column(Integer, primary_key=True)
            rel = relationship("TestModel2", uselist=False, back_populates="rel")
        class TestModel2(Base):
            __tablename__ = "test2"
            id = Column(Integer, primary_key=True)
            fk = Column(Integer, ForeignKey(TestModel1.id))
            rel = relationship("TestModel1", uselist=False, back_populates="rel")
        m1 = TestModel1(id=1)
        m2 = TestModel2(id=2, fk=1)
        m1.rel = m2
        m2.rel = m1
        rel = TestModel1.__mapper__.relationships.rel
        val = list(fe._serialize_relationship(m1, rel, "rel", set()))
        correct = [("rel", {"id": 2, "fk": 1})]
        assert val == correct

    @staticmethod
    def test_self_relationship(Base):
        """
        _serialize_relationship() should still work when a table has a
        relationship with itself.
        """
        # pylint:disable=missing-docstring
        class TestModel(Base):
            __tablename__ = "test"
            id = Column(Integer, primary_key=True)
            sk = Column(Integer, ForeignKey("test.id"))
            rel = relationship("TestModel", uselist=False)
        m1 = TestModel(id=1)
        m2 = TestModel(id=2, sk=1, rel=m1)
        rel = TestModel.__mapper__.relationships.rel
        val = list(fe._serialize_relationship(m2, rel, "rel", set()))
        correct = [("rel", {"id": 1, "sk": None})]
        assert val == correct

    @staticmethod
    def test_cyclic_relationship(Base):
        """
        Test A -> B -> C -> A -> B ... The cycle breaker should cause
        a placeholder to replace the 2nd B (not the A, since C -> A
        still hasn't been seen yet).
        """
        # pylint:disable=missing-docstring
        # pylint:disable=bad-continuation
        class TestModel(Base):
            __tablename__ = "test"
            id = Column(Integer, primary_key=True)
            sk = Column(Integer, ForeignKey("test.id"))
            rel = relationship("TestModel", uselist=False, lazy=False)
        m1 = TestModel(id=1, sk=2)
        m2 = TestModel(id=2, sk=3)
        m3 = TestModel(id=3, sk=1, rel=m1)
        m1.rel = m2
        m2.rel = m3
        rel = TestModel.__mapper__.relationships.rel
        val = list(fe._serialize_relationship(m1, rel, "rel", set()))
        placeholder = fe.ModelPlaceholder(m2)
        # Note: From 1, we start at 2...
        correct = [("rel", {
            "id": 2, "sk": 3, "rel": {
                "id": 3, "sk": 1, "rel": {
                    "id": 1, "sk": 2, "rel": placeholder
                }
            }
        })]
        assert val == correct

    @staticmethod
    def test_serialize_one_to_many(Base):
        """Test basic serialization of one-to-many relationship"""
        # pylint:disable=missing-docstring
        class TestModel1(Base):
            __tablename__ = "test1"
            id = Column(Integer, primary_key=True)
            rel = relationship("TestModel2", uselist=True)
        class TestModel2(Base):
            __tablename__ = "test2"
            id = Column(Integer, primary_key=True)
            fk = Column(Integer, ForeignKey(TestModel1.id))
        m1 = TestModel1(id=1)
        m2s = [TestModel2(id=2, fk=1), TestModel2(id=3, fk=1)]
        m1.rel = m2s
        rel = TestModel1.__mapper__.relationships.rel
        val = list(fe._serialize_relationship(m1, rel, "rel", set()))
        correct = [("rel", [{"id": 2, "fk": 1}, {"id": 3, "fk": 1}])]
        assert val == correct

    @staticmethod
    def test_serialize_many_to_many(Base):
        """Test serialization of many-to-many relationship"""
        # pylint:disable=missing-docstring
        # pylint:disable=bad-continuation
        rel_table = Table("rel", Base.metadata,
            Column("fk1", Integer, ForeignKey("test1.id"), primary_key=True),
            Column("fk2", Integer, ForeignKey("test2.id"), primary_key=True),
        )
        class TestModel1(Base):
            __tablename__ = "test1"
            id = Column(Integer, primary_key=True)
            rel = relationship("TestModel2", secondary=rel_table, lazy=False)
        class TestModel2(Base):
            __tablename__ = "test2"
            id = Column(Integer, primary_key=True)
            rel = relationship("TestModel1", secondary=rel_table, lazy=False)
        m1s = [TestModel1(id=1), TestModel1(id=2), TestModel1(id=3)]
        m2s = [TestModel2(id=11), TestModel2(id=12), TestModel2(id=13)]
        m1s[0].rel = [m2s[0], m2s[1]] # 1 -> 11, 1 -> 12
        m1s[1].rel = [m2s[2]] # 2 -> 13
        m1s[2].rel = []
        m2s[0].rel = []
        m2s[1].rel = [m1s[1]] # 12 -> 2
        m2s[2].rel = [m1s[0], m1s[2]] # 13 -> 1, 13 -> 3
        # These relationships cause all 6 objects to be serialized, in this order:
        # 1 -> 11, 1 -> 12, 12 -> 2, 2 -> 13, 13 -> 1, 13 -> 3
        # The cycle is broken after 13 -> 1; 1's relationships become placeholders
        # the 2nd time around.
        rel = TestModel1.__mapper__.relationships.rel
        val = list(fe._serialize_relationship(m1s[0], rel, "rel", set()))
        placeholders = [fe.ModelPlaceholder(m2s[0]), fe.ModelPlaceholder(m2s[1])]
        # Here's the complicated answer:
        correct = [("rel", [
            {"id": 11, "rel": []},
            {"id": 12, "rel": [
                {"id": 2, "rel": [
                    {"id": 13, "rel": [
                        {"id": 1, "rel": placeholders},
                        {"id": 3, "rel": []}
                    ]}
                ]}
            ]}
        ])]
        assert val == correct


class TestSerializerInterface:
    """
    Tests of the external serialize_model_object() interface.
    """

    @staticmethod
    def test_model1_with_default_columns(Model1):
        """
        Test external interface on Model1. No special args.
        """
        m = Model1(id=1, col7=42, col11="test")
        result = fe.serialize_model_object(m)
        assert all(f"col{n}" in result for n in range(1, 14))
        assert result["id"] == 1
        assert result["col7"] == 42
        assert result["col11"] == "test"

    @staticmethod
    def test_model1_with_specific_columns(Model1):
        """
        Test external interface on Model1. Specify columns.
        """
        m = Model1(id=1, col7=42, col11="test")
        cols = {"id": "out1", "col7": "out2", "col11": "out3"}
        result = fe.serialize_model_object(m, columns=cols)
        assert "id" not in result
        assert not any(f"col{n}" in result for n in range(1, 14))
        assert result == {"out1": 1, "out2": 42, "out3": "test"}

    @staticmethod
    def test_model2_with_default_relationships(Model1, Model2):
        """
        Test external interface on Model2. No special args.
        """
        m1s = [Model1(id=i) for i in range(1, 6)]
        kwargs = {f"fk{i}": i for i in range(1, 6)}
        kwargs.update({f"rel{i}": m1s[i - 1] for i in range(1, 6)})
        m2 = Model2(id=1, **kwargs)
        result = fe.serialize_model_object(m2)
        assert result["id"] == 1
        assert all(f"fk{i}" in result for i in range(1, 13))
        assert all(result[f"rel{i}"]["id"] == i for i in range(1, 6))

    @staticmethod
    def test_model2_with_specific_relationships(Model1, Model2):
        """
        Test external interface on Model2. Specify columns & relationships.
        Try specifying no columns (may replicate above in a simpler test).
        Specify both a "loaded" and a "non-loaded" relationship.
        """
        m1s = [Model1(id=i) for i in range(1, 6)]
        kwargs = {f"fk{i}": i for i in range(1, 6)}
        kwargs.update({f"rel{i}": m1s[i - 1] for i in range(1, 6)})
        m2 = Model2(id=1, **kwargs)
        rels = {"rel1": "out1", "rel11": "out2"}
        result = fe.serialize_model_object(m2, columns=[], relationships=rels)
        assert "id" not in result
        assert not any(f"fk{i}" in result for i in range(1, 13))
        assert not any(f"rel{i}" in result for i in range(1, 13))
        assert result["out1"]["id"] == 1
        assert result["out2"] is None

    @staticmethod
    def test_model3_with_default_attributes(Model3):
        """
        Test external interface on Model3. No special args.
        """
        m3 = Model3(id=1)
        result = fe.serialize_model_object(m3)
        assert all(x in result for x in ("id", "fk"))
        assert not any(f"attr{i}" in result for i in range(1, 5))
        assert "__str__" not in result

    @staticmethod
    def test_model3_with_specific_attributes(Model3):
        """
        Test external interface on Model3. Specify all attributes.
        """
        m3 = Model3(id=1)
        attrs = ["attr1", "attr2", "attr3", "_Model3__attr4", "__str__"]
        result = fe.serialize_model_object(m3, columns=[], attributes=attrs)
        assert all(x in result for x in attrs)
        assert all(result[x] == 42 for x in ("attr1", "attr2", "attr3"))
        assert result["_Model3__attr4"] is None
        assert result["__str__"] == "Model3"
